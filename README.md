# README #

A list of files demostrating how to use jupyter notebooks

### How do I get set up? ###

> jupyter notebook


## Resources ##

1. https://pandas.pydata.org/pandas-docs/stable/basics.html

2. https://pandas.pydata.org/pandas-docs/stable/tutorials.html

3. http://jupyter.org/

3.1. https://www.datacamp.com/community/tutorials/tutorial-jupyter-notebook
https://jupyter.readthedocs.io/en/latest/

